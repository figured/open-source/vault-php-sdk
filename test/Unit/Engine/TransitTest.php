<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Test\Unit\Engine;

use Exception;
use FiguredLimited\VaultSdk\Client;
use FiguredLimited\VaultSdk\Engine\Transit;
use FiguredLimited\VaultSdk\Exception\InternalServerErrorException;
use FiguredLimited\VaultSdk\Exception\InvalidCiphertextException;
use FiguredLimited\VaultSdk\Response;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

class TransitTest extends TestCase
{
    private const BASE_PATH  = 'test-transit';
    private const KEY_NAME   = 'test-key';
    private const PLAINTEXT  = 'foo';
    private const CIPHERTEXT = 'vault:v1:YmFy';

    private Client|MockInterface $client;
    private Transit $sut;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = Mockery::mock(Client::class);

        $this->sut = new Transit($this->client, self::BASE_PATH);
    }

    public function testEncryptWithoutBase64(): void
    {
        $this->expectEncryptPostSent();
        $result = $this->sut->encrypt(self::KEY_NAME, self::PLAINTEXT);

        $this->assertSame(self::CIPHERTEXT, $result);
    }

    public function testEncryptWithBase64EncodedString(): void
    {
        $this->expectEncryptPostSent();
        $result = $this->sut->encrypt(self::KEY_NAME, base64_encode(self::PLAINTEXT), true);

        $this->assertSame(self::CIPHERTEXT, $result);
    }

    public function testDecryptWithSuccessfulDecryptionAndReturnedAsBase64(): void
    {
        $this->expectDecryptPostSent();
        $result = $this->sut->decrypt(self::KEY_NAME, self::CIPHERTEXT, true);

        $this->assertSame(base64_encode(self::PLAINTEXT), $result);
    }

    public function testDecryptWithSuccessfulDecryptionAndReturnedAsPlaintext(): void
    {
        $this->expectDecryptPostSent();
        $result = $this->sut->decrypt(self::KEY_NAME, self::CIPHERTEXT);

        $this->assertSame(self::PLAINTEXT, $result);
    }

    public function testDecryptWithInvalidMac(): void
    {
        $this->expectException(InvalidCiphertextException::class);
        $this->expectExceptionMessage('Failed to decode ciphertext');

        $this->expectDecryptPostSentAndThrowException(new InternalServerErrorException(
            json_encode(['errors' => ['1 error occurred:\n\t* cipher: message authentication failed\n\n']]),
            500
        ));

        $this->sut->decrypt(self::KEY_NAME, self::CIPHERTEXT);
    }

    /**
     * @dataProvider getDecryptExceptions
     */
    public function testDecryptWithException(Exception $exception): void
    {
        $this->expectExceptionObject($exception);

        $this->expectDecryptPostSentAndThrowException($exception);

        $this->sut->decrypt(self::KEY_NAME, self::CIPHERTEXT);
    }

    public function getDecryptExceptions(): array
    {
        return [
            [new Exception('foo')],
            [new InternalServerErrorException('[foo]', 500)],
        ];
    }

    private function expectEncryptPostSent(): void
    {
        $payload    = [
            'plaintext' => base64_encode(self::PLAINTEXT),
        ];
        $resultData = [
            'ciphertext' => self::CIPHERTEXT,
        ];
        $this->expectPostSent('/v1/' . self::BASE_PATH . '/encrypt/' . self::KEY_NAME, $payload, $resultData);
    }

    private function expectDecryptPostSent(): void
    {
        $payload    = [
            'ciphertext' => self::CIPHERTEXT,
        ];
        $resultData = [
            'plaintext' => base64_encode(self::PLAINTEXT),
        ];
        $this->expectPostSent('/v1/' . self::BASE_PATH . '/decrypt/' . self::KEY_NAME, $payload, $resultData);
    }

    private function expectDecryptPostSentAndThrowException(Exception $e): void
    {
        $payload    = [
            'ciphertext' => self::CIPHERTEXT,
        ];

        $this->client->shouldReceive('sendPost')
            ->once()
            ->with('/v1/' . self::BASE_PATH . '/decrypt/' . self::KEY_NAME, $payload)
            ->andThrow($e);
    }

    private function expectPostSent(string $path, array $payload, array $resultData)
    {
        $response = Mockery::mock(Response::class);

        $response->shouldReceive('getData')
            ->withNoArgs()
            ->andReturn($resultData);

        $this->client->shouldReceive('sendPost')
            ->once()
            ->with($path, $payload)
            ->andReturn($response);
    }
}
