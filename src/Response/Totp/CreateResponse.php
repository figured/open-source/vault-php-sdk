<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Response\Totp;

use FiguredLimited\VaultSdk\Exception\Totp\NoBarcodeInResponseException;

class CreateResponse
{
    public function __construct(public readonly string $url, public readonly ?string $barcode)
    {
    }

    public function getBarcodeImageContent(): string
    {
        $this->ensureResponseHasBarcode();

        return base64_decode($this->barcode);
    }

    public function getBarcodeDataUri(): string
    {
        $this->ensureResponseHasBarcode();

        return 'data:image/png;base64,' . $this->barcode;
    }

    private function ensureResponseHasBarcode(): void
    {
        if (null === $this->barcode) {
            throw new NoBarcodeInResponseException('No barcode was sent in the response');
        }
    }
}
