<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk;

use FiguredLimited\VaultSdk\Exception\BadRequestException;
use FiguredLimited\VaultSdk\Exception\ClientException;
use FiguredLimited\VaultSdk\Exception\ForbiddenException;
use FiguredLimited\VaultSdk\Exception\InternalServerErrorException;
use FiguredLimited\VaultSdk\Exception\NotFoundException;
use FiguredLimited\VaultSdk\Exception\UnhandledStatusCodeException;
use JsonException;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriInterface;

class Client
{
    public function __construct(
        protected ClientInterface $client,
        protected RequestFactoryInterface $requestFactory,
        protected StreamFactoryInterface $streamFactory,
        protected UriInterface $baseUrl,
        protected ?string $token
    ) {
    }

    /**
     * @throws JsonException
     * @throws ClientExceptionInterface
     * @throws ClientException
     */
    public function sendPost(string $path, array $payload, array $headers = []): ?Response
    {
        $uri     = $this->baseUrl->withPath($path);
        $request = $this->requestFactory->createRequest('POST', $uri)
            ->withBody($this->streamFactory->createStream(json_encode($payload, JSON_THROW_ON_ERROR)));

        foreach ($headers as $header => $value) {
            $request = $request->withHeader($header, $value);
        }

        if (null !== $this->token) {
            $request = $request->withHeader('X-Vault-Token', $this->token);
        }

        return $this->sendRequestAndHandleResponse($request);
    }

    /**
     * @throws JsonException
     * @throws ClientExceptionInterface
     * @throws ClientException
     */
    public function sendGet(string $path, array $headers = []): ?Response
    {
        $uri     = $this->baseUrl->withPath($path);
        $request = $this->requestFactory->createRequest('GET', $uri);

        foreach ($headers as $header => $value) {
            $request = $request->withHeader($header, $value);
        }

        if (null !== $this->token) {
            $request = $request->withHeader('X-Vault-Token', $this->token);
        }

        return $this->sendRequestAndHandleResponse($request);
    }

    private function sendRequestAndHandleResponse(RequestInterface $request): ?Response
    {
        $response = $this->client->sendRequest($request);

        switch ($response->getStatusCode()) {
            case 200:
            case 201:
                return new Response($response);

            case 204:
                return null;

            case 400:
                throw new BadRequestException((string) $response->getBody(), $response->getStatusCode());

            case 403:
                throw new ForbiddenException((string) $response->getBody(), $response->getStatusCode());

            case 404:
                throw new NotFoundException((string) $response->getBody(), $response->getStatusCode());

            case 500:
                throw new InternalServerErrorException((string) $response->getBody(), $response->getStatusCode());

            default:
                throw new UnhandledStatusCodeException((string) $response->getBody(), $response->getStatusCode());
        }
    }
}
