<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Test\Unit\Engine;

use FiguredLimited\VaultSdk\Client;
use FiguredLimited\VaultSdk\Engine\Totp;
use FiguredLimited\VaultSdk\Exception\BadRequestException;
use FiguredLimited\VaultSdk\Exception\Totp\CodeAlreadyUsedException;
use FiguredLimited\VaultSdk\Request\Totp\CreateRequestInterface;
use FiguredLimited\VaultSdk\Response;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

class TotpTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    private const BASE_PATH = 'test-transit';
    private const KEY_NAME  = 'test-key';

    private const OTP_URL = 'otpauth://totp/Google:test@gmail.com?algorithm=SHA1&digits=6&issuer=Google&period=30&secret=HTXT7KJFVNAJUPYWQRWMNVQE5AF5YZI2';
    private const BARCODE = 'testBarcode';
    private const CODE    = '123456';

    private const CREATE_PAYLOAD  = ['url' => self::OTP_URL];
    private const CREATE_RESPONSE = [
        'url'     => self::OTP_URL,
        'barcode' => self::BARCODE,
    ];

    private Client|MockInterface $client;
    private Totp                 $sut;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = Mockery::mock(Client::class);

        $this->sut = new Totp($this->client, self::BASE_PATH);
    }

    public function testCreateKeyWithResponsePayload_shouldReturnTheResponse(): void
    {
        $request = $this->getCreateRequest();
        $this->expectCreatePostSent($this->getClientResponse(self::CREATE_RESPONSE));
        $result = $this->sut->createKey(self::KEY_NAME, $request);

        $this->assertSame(self::OTP_URL, $result->url);
        $this->assertSame(self::BARCODE, $result->barcode);
    }

    public function testCreateKeyWithoutResponsePayload_shouldReturnNull(): void
    {
        $request = $this->getCreateRequest();
        $this->expectCreatePostSent(null);
        $this->assertNull($this->sut->createKey(self::KEY_NAME, $request));
    }

    public function testGenerateCode(): void
    {
        $this->expectGenerateCodeGetSent();
        $this->sut->generateCode(self::KEY_NAME);
    }

    public function testVerifyCodeWithValidCode_shouldReturnTrue(): void
    {
        $this->expectVerifyCodePostSent();
        $this->sut->verifyCode(self::KEY_NAME, self::CODE);
    }

    public function testVerifyCodeWithReusedCode_shouldThrowException(): void
    {
        $this->expectException(CodeAlreadyUsedException::class);

        $this->expectVerifyCodePostSentWithReusedCode();
        $this->sut->verifyCode(self::KEY_NAME, self::CODE);
    }

    private function getClientResponse(array $responseData): Response
    {
        $response = Mockery::mock(Response::class);

        $response->shouldReceive('getData')
            ->withNoArgs()
            ->andReturn($responseData);

        return $response;
    }

    private function expectCreatePostSent(?Response $response): void
    {
        $this->expectPostSent(
            '/v1/' . self::BASE_PATH . '/keys/' . self::KEY_NAME,
            self::CREATE_PAYLOAD,
            $response,
        );
    }

    private function expectGenerateCodeGetSent(): void
    {
        $this->expectGetSent(
            '/v1/' . self::BASE_PATH . '/code/' . self::KEY_NAME,
            $this->getClientResponse(['code' => self::CODE]),
        );
    }

    private function expectVerifyCodePostSent(): void
    {
        $this->expectPostSent(
            '/v1/' . self::BASE_PATH . '/code/' . self::KEY_NAME,
            ['code' => self::CODE],
            $this->getClientResponse(['valid' => true]),
        );
    }

    private function expectVerifyCodePostSentWithReusedCode(): void
    {
        $path      = '/v1/' . self::BASE_PATH . '/code/' . self::KEY_NAME;
        $payload   = ['code' => self::CODE];
        $exception = $this->getReusedCodeBadRequestException();

        $this->client->shouldReceive('sendPost')
            ->once()
            ->with($path, $payload)
            ->andThrow($exception);
    }

    private function getReusedCodeBadRequestException(): BadRequestException
    {
        $exception = Mockery::mock(BadRequestException::class);

        $exception->shouldReceive('getBody')
            ->andReturn('{"errors":["code already used; wait until the next time period"]}');

        $exception->shouldReceive('getStatusCode')
            ->andReturn(400);

        $exception->shouldReceive('get');

        return $exception;
    }

    private function expectPostSent(string $path, array $payload, ?Response $response)
    {
        $this->client->shouldReceive('sendPost')
            ->once()
            ->with($path, $payload)
            ->andReturn($response);
    }

    private function expectGetSent(string $path, ?Response $response)
    {
        $this->client->shouldReceive('sendGet')
            ->once()
            ->with($path)
            ->andReturn($response);
    }

    private function getCreateRequest(): CreateRequestInterface
    {
        $request = Mockery::mock(CreateRequestInterface::class);

        $request->shouldReceive('toPayload')
            ->once()
            ->andReturn(self::CREATE_PAYLOAD);

        return $request;
    }
}
