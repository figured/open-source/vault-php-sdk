<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Exception;

use Throwable;

class ClientException extends \Exception
{
    protected string $body;
    protected int $statusCode;

    public function __construct(string $body, int $statusCode, string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->body       = $body;
        $this->statusCode = $statusCode;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}
