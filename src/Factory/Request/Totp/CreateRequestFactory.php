<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Factory\Request\Totp;

use FiguredLimited\VaultSdk\Enum\Totp\Algorithm;
use FiguredLimited\VaultSdk\Enum\Totp\Digits;
use FiguredLimited\VaultSdk\Request\Totp\CreateForConsumerByKeyRequest;
use FiguredLimited\VaultSdk\Request\Totp\CreateForConsumerByUrlRequest;
use FiguredLimited\VaultSdk\Request\Totp\CreateForProducerRequest;

class CreateRequestFactory
{
    public function createForConsumerByUrl(string $url): CreateForConsumerByUrlRequest
    {
        return new CreateForConsumerByUrlRequest($url);
    }

    public function createForConsumerByKey(
        string $key,
        string $issuer = '',
        string $accountName = '',
        int $periodSeconds = 30,
        Algorithm $algorithm = Algorithm::SHA1,
        Digits $digits = Digits::SIX,
    ): CreateForConsumerByKeyRequest {
        return new CreateForConsumerByKeyRequest($key, $issuer, $accountName, $periodSeconds, $algorithm, $digits);
    }

    public function createForProducer(
        string $issuer,
        string $accountName,
        Algorithm $algorithm = Algorithm::SHA1,
        int $keySize = 20,
        int $periodSeconds = 30,
        Digits $digits = Digits::SIX,
        int $qrSize = 200,
        bool $exported = true,
        int $skew = 1,
    ): CreateForProducerRequest {
        return new CreateForProducerRequest(
            $issuer,
            $accountName,
            $algorithm,
            $keySize,
            $periodSeconds,
            $digits,
            $qrSize,
            $exported,
            $skew,
        );
    }
}
