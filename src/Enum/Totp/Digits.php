<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Enum\Totp;

enum Digits: int
{
    case SIX   = 6;
    case EIGHT = 8;
}
