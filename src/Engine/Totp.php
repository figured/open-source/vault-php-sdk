<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Engine;

use FiguredLimited\VaultSdk\Client;
use FiguredLimited\VaultSdk\Exception\BadRequestException;
use FiguredLimited\VaultSdk\Exception\ClientException;
use FiguredLimited\VaultSdk\Exception\Totp\CodeAlreadyUsedException;
use FiguredLimited\VaultSdk\Request\Totp\CreateRequestInterface;
use FiguredLimited\VaultSdk\Response\Totp\CreateResponse;
use JsonException;
use Psr\Http\Client\ClientExceptionInterface;

class Totp
{
    protected Client $client;
    protected string $basePath;

    public function __construct(Client $client, string $basePath = 'totp')
    {
        $this->client   = $client;
        $this->basePath = $basePath;
    }

    /**
     * @throws ClientException
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function createKey(string $name, CreateRequestInterface $request): ?CreateResponse
    {
        $path = sprintf('/v1/%s/keys/%s', $this->basePath, $name);

        $response = $this->client->sendPost($path, $request->toPayload());

        if (null === $response) {
            return null;
        }

        $data = $response->getData();

        return new CreateResponse($data['url'], $data['barcode'] ?? null);
    }

    /**
     * @throws ClientException
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function generateCode(string $name): string
    {
        $path = sprintf('/v1/%s/code/%s', $this->basePath, $name);

        $response = $this->client->sendGet($path);

        return $response->getData()['code'];
    }

    /**
     * @throws ClientException
     * @throws ClientExceptionInterface
     * @throws CodeAlreadyUsedException
     * @throws JsonException
     */
    public function verifyCode(string $name, string $code): bool
    {
        $path = sprintf('/v1/%s/code/%s', $this->basePath, $name);

        try {
            $response = $this->client->sendPost($path, ['code' => $code]);
        } catch (BadRequestException $e) {
            $body = json_decode($e->getBody(), true);

            if (
                !empty($body)
                && !empty($body['errors'])
                && str_contains($body['errors'][0], 'code already used')
            ) {
                throw new CodeAlreadyUsedException(
                    $e->getBody(),
                    $e->getStatusCode(),
                    $e->getMessage(),
                    $e->getCode(),
                    $e,
                );
            }

            throw $e;
        }

        return $response->getData()['valid'];
    }
}
