<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Enum\Totp;

enum Algorithm: string
{
    case SHA1   = 'SHA1';
    case SHA256 = 'SHA256';
    case SHA512 = 'SHA512';
}
