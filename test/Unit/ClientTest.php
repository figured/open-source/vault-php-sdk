<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Test\Unit;

use FiguredLimited\VaultSdk\Client;
use FiguredLimited\VaultSdk\Exception\BadRequestException;
use FiguredLimited\VaultSdk\Exception\ForbiddenException;
use FiguredLimited\VaultSdk\Exception\InternalServerErrorException;
use FiguredLimited\VaultSdk\Exception\NotFoundException;
use FiguredLimited\VaultSdk\Exception\UnhandledStatusCodeException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

class ClientTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    private const BASE_URL      = 'http://localhost:8200';
    private const TOKEN         = 's.test';
    private const PATH          = '/v1/transit/test/encrypt';
    private const PAYLOAD       = [
        'plaintext' => 'foo',
    ];
    private const HEADERS       = [
        'X-Test' => 'bar',
    ];
    private const RESPONSE_BODY = [
        'request_id'     => 'test',
        'lease_id'       => '',
        'renewable'      => false,
        'lease_duration' => 0,
        'data'           => [
            'cyphertext' => 'foo',
        ],
        'wrap_info'      => null,
        'warnings'       => null,
        'auth'           => null,
    ];

    /** @var MockInterface|ClientInterface */
    private $client;
    /** @var MockInterface|RequestFactoryInterface */
    private $requestFactory;
    /** @var MockInterface|StreamFactoryInterface */
    private $streamFactory;
    /** @var Uri */
    private $baseUrl;
    /** @var Client */
    private $sut;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client         = Mockery::mock(ClientInterface::class);
        $this->requestFactory = Mockery::mock(RequestFactoryInterface::class);
        $this->streamFactory  = Mockery::mock(StreamFactoryInterface::class);
        $this->baseUrl        = new Uri(self::BASE_URL);

        $this->sut = new Client(
            $this->client,
            $this->requestFactory,
            $this->streamFactory,
            $this->baseUrl,
            self::TOKEN
        );
    }

    /**
     * @dataProvider getSuccessfulStatusCodes
     */
    public function testSendPostWithSuccessfulStatusCode(int $statusCode): void
    {
        $expectedHeaders                  = self::HEADERS;
        $expectedHeaders['X-Vault-Token'] = self::TOKEN;

        $request = $this->expectRequestCreated('POST', self::BASE_URL . self::PATH);
        $this->expectBodySetToRequest($request, json_encode(self::PAYLOAD));
        $this->expectHeadersSet($request, $expectedHeaders);

        $response = $this->expectRequestSent($request, $statusCode);

        $this->assertSame(
            $response,
            $this->sut->sendPost(self::PATH, self::PAYLOAD, self::HEADERS)->getRawResponse()
        );
    }

    public function testSendPostWithSuccessfulEmptyStatusCode(): void
    {
        $expectedHeaders                  = self::HEADERS;
        $expectedHeaders['X-Vault-Token'] = self::TOKEN;

        $request = $this->expectRequestCreated('POST', self::BASE_URL . self::PATH);
        $this->expectBodySetToRequest($request, json_encode(self::PAYLOAD));
        $this->expectHeadersSet($request, $expectedHeaders);

        $this->expectRequestSent($request, 204);

        $this->assertNull($this->sut->sendPost(self::PATH, self::PAYLOAD, self::HEADERS));
    }

    /**
     * @dataProvider getErrorStatusCodesAndExceptionClasses
     */
    public function testSendPostWithErrorStatusCode(int $statusCode, string $exceptionClassName): void
    {
        $this->expectException($exceptionClassName);

        $expectedHeaders                  = self::HEADERS;
        $expectedHeaders['X-Vault-Token'] = self::TOKEN;

        $request    = $this->expectRequestCreated('POST', self::BASE_URL . self::PATH);
        $this->expectBodySetToRequest($request, json_encode(self::PAYLOAD));
        $this->expectHeadersSet($request, $expectedHeaders);

        $this->expectRequestSent($request, $statusCode);

        $this->sut->sendPost(self::PATH, self::PAYLOAD, self::HEADERS);
    }

    /**
     * @dataProvider getSuccessfulStatusCodes
     */
    public function testSendGetWithSuccessfulStatusCode(int $statusCode): void
    {
        $expectedHeaders                  = self::HEADERS;
        $expectedHeaders['X-Vault-Token'] = self::TOKEN;

        $request = $this->expectRequestCreated('GET', self::BASE_URL . self::PATH);
        $this->expectHeadersSet($request, $expectedHeaders);

        $response = $this->expectRequestSent($request, $statusCode);

        $this->assertSame(
            $response,
            $this->sut->sendGet(self::PATH, self::HEADERS)->getRawResponse()
        );
    }

    public function testSendGetWithSuccessfulEmptyStatusCode(): void
    {
        $expectedHeaders                  = self::HEADERS;
        $expectedHeaders['X-Vault-Token'] = self::TOKEN;

        $request = $this->expectRequestCreated('GET', self::BASE_URL . self::PATH);
        $this->expectHeadersSet($request, $expectedHeaders);

        $this->expectRequestSent($request, 204);

        $this->assertNull($this->sut->sendGet(self::PATH, self::HEADERS));
    }

    /**
     * @dataProvider getErrorStatusCodesAndExceptionClasses
     */
    public function testSendGetWithErrorStatusCode(int $statusCode, string $exceptionClassName): void
    {
        $this->expectException($exceptionClassName);

        $expectedHeaders                  = self::HEADERS;
        $expectedHeaders['X-Vault-Token'] = self::TOKEN;

        $request    = $this->expectRequestCreated('GET', self::BASE_URL . self::PATH);
        $this->expectHeadersSet($request, $expectedHeaders);

        $this->expectRequestSent($request, $statusCode);

        $this->sut->sendGet(self::PATH, self::HEADERS);
    }

    public function getSuccessfulStatusCodes(): array
    {
        return [
            [200],
            [201],
        ];
    }

    public function getErrorStatusCodesAndExceptionClasses(): array
    {
        return [
            [400, BadRequestException::class],
            [403, ForbiddenException::class],
            [404, NotFoundException::class],
            [500, InternalServerErrorException::class],
            [402, UnhandledStatusCodeException::class],
        ];
    }

    /**
     * @return RequestInterface|MockInterface
     */
    private function expectRequestCreated(string $method, string $url): RequestInterface
    {
        $request = Mockery::mock(RequestInterface::class);

        $this->requestFactory->shouldReceive('createRequest')
            ->once()
            ->with(
                $method,
                Mockery::on(
                    function (UriInterface $uri) use ($url) {
                        return $url === (string) $uri;
                    }
                )
            )
            ->andReturn($request);

        return $request;
    }

    /**
     * @param RequestInterface|MockInterface $request
     *
     * @return StreamInterface|MockInterface
     */
    private function expectBodySetToRequest(RequestInterface $request, string $body): StreamInterface
    {
        $bodyStream = Mockery::mock(StreamInterface::class);

        $bodyStream->shouldReceive('__toString')
            ->withNoArgs()
            ->andReturn($body);

        $this->streamFactory->shouldReceive('createStream')
            ->once()
            ->with($body)
            ->andReturn($bodyStream);

        // Though implementations of the request interface are supposed to be immutable, for simplicity's sake we'll ignore that
        $request->shouldReceive('withBody')
            ->once()
            ->with($bodyStream)
            ->andReturnSelf();

        return $bodyStream;
    }

    /**
     * @param RequestInterface|MockInterface $request
     */
    private function expectHeadersSet(RequestInterface $request, array $headers): void
    {
        // Though implementations of the request interface are supposed to be immutable, for simplicity's sake we'll ignore that
        foreach ($headers as $name => $value) {
            $request->shouldReceive('withHeader')
                ->once()
                ->with($name, $value)
                ->andReturnSelf();
        }
    }

    private function expectRequestSent(RequestInterface $request, int $statusCode): ResponseInterface
    {
        $response = new Response($statusCode, ['Content-Type' => 'application/json'], json_encode(self::RESPONSE_BODY));

        $this->client->shouldReceive('sendRequest')
            ->with($request)
            ->andReturn($response);

        return $response;
    }
}
