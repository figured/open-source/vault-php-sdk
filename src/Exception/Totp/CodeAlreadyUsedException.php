<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Exception\Totp;

use FiguredLimited\VaultSdk\Exception\ClientException;

class CodeAlreadyUsedException extends ClientException
{
}
