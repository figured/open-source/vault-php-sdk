<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Request\Totp;

class CreateForConsumerByUrlRequest implements CreateRequestInterface
{
    public function __construct(private readonly string $url)
    {
    }

    public function toPayload(): array
    {
        return [
            'generate' => false,
            'url'      => $this->url,
        ];
    }
}
