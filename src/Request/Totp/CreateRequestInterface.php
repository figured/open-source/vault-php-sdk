<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Request\Totp;

interface CreateRequestInterface
{
    public function toPayload(): array;
}
