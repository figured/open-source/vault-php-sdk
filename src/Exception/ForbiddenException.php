<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Exception;

class ForbiddenException extends ClientException
{
}
