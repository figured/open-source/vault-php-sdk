<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Engine;

use FiguredLimited\VaultSdk\Client;
use FiguredLimited\VaultSdk\Exception\ClientException;
use FiguredLimited\VaultSdk\Exception\InternalServerErrorException;
use FiguredLimited\VaultSdk\Exception\InvalidCiphertextException;
use JsonException;
use Psr\Http\Client\ClientExceptionInterface;

class Transit
{
    protected Client $client;
    protected string $basePath;

    public function __construct(Client $client, string $basePath = 'transit')
    {
        $this->client   = $client;
        $this->basePath = $basePath;
    }

    /**
     * @throws ClientException
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function encrypt(string $keyName, string $plaintext, bool $isBase64Encoded = false): string
    {
        $path = sprintf('/v1/%s/encrypt/%s', $this->basePath, $keyName);

        $payload = [
            'plaintext' => $isBase64Encoded ? $plaintext : base64_encode($plaintext),
        ];

        $response = $this->client->sendPost($path, $payload);

        return $response->getData()['ciphertext'];
    }

    /**
     * @throws InvalidCiphertextException
     * @throws ClientException
     * @throws JsonException
     * @throws ClientExceptionInterface
     */
    public function decrypt(string $keyName, string $ciphertext, bool $returnAsBase64 = false): string
    {
        $path = sprintf('/v1/%s/decrypt/%s', $this->basePath, $keyName);

        $payload = [
            'ciphertext' => $ciphertext,
        ];

        try {
            $response = $this->client->sendPost($path, $payload);
        } catch (InternalServerErrorException $e) {
            $body = json_decode($e->getBody(), true);

            if (!empty($body) && false !== strstr($body['errors'][0] ?? '', 'cipher: message authentication failed')) {
                throw new InvalidCiphertextException('Failed to decode ciphertext', 0, $e);
            }

            throw $e;
        }

        $plaintext =  $response->getData()['plaintext'];

        return $returnAsBase64 ? $plaintext : base64_decode($plaintext);
    }
}
