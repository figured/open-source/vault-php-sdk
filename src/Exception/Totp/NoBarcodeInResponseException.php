<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Exception\Totp;

use RuntimeException;

class NoBarcodeInResponseException extends RuntimeException
{
}
