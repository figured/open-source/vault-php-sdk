<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Request\Totp;

use FiguredLimited\VaultSdk\Enum\Totp\Algorithm;
use FiguredLimited\VaultSdk\Enum\Totp\Digits;

class CreateForProducerRequest implements CreateRequestInterface
{
    public function __construct(
        private readonly string $issuer,
        private readonly string $accountName,
        private Algorithm $algorithm = Algorithm::SHA1,
        private int $keySize = 20,
        private int $periodSeconds = 30,
        private Digits $digits = Digits::SIX,
        private int $qrSize = 200,
        private bool $exported = true,
        private int $skew = 1,
    ) {
    }

    public function isExported(): bool
    {
        return $this->exported;
    }

    public function setExported(bool $exported): CreateForProducerRequest
    {
        $this->exported = $exported;

        return $this;
    }

    public function getKeySize(): int
    {
        return $this->keySize;
    }

    public function setKeySize(int $keySize): CreateForProducerRequest
    {
        $this->keySize = $keySize;

        return $this;
    }

    public function getPeriodSeconds(): int
    {
        return $this->periodSeconds;
    }

    public function setPeriodSeconds(int $periodSeconds): CreateForProducerRequest
    {
        $this->periodSeconds = $periodSeconds;

        return $this;
    }

    public function getAlgorithm(): Algorithm
    {
        return $this->algorithm;
    }

    public function setAlgorithm(Algorithm $algorithm): CreateForProducerRequest
    {
        $this->algorithm = $algorithm;

        return $this;
    }

    public function getDigits(): Digits
    {
        return $this->digits;
    }

    public function setDigits(Digits $digits): CreateForProducerRequest
    {
        $this->digits = $digits;

        return $this;
    }

    public function getSkew(): int
    {
        return $this->skew;
    }

    public function setSkew(int $skew): CreateForProducerRequest
    {
        $this->skew = $skew;

        return $this;
    }

    public function getQrSize(): int
    {
        return $this->qrSize;
    }

    public function setQrSize(int $qrSize): CreateForProducerRequest
    {
        $this->qrSize = $qrSize;

        return $this;
    }

    public function toPayload(): array
    {
        return [
            'generate'     => true,
            'issuer'       => $this->issuer,
            'account_name' => $this->accountName,
            'exported'     => $this->exported,
            'key_size'     => $this->keySize,
            'period'       => $this->periodSeconds,
            'algorithm'    => $this->algorithm->value,
            'digits'       => $this->digits->value,
            'skew'         => $this->skew,
            'qr_size'      => $this->qrSize,
        ];
    }
}
