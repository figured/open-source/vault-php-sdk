<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Request\Totp;

use FiguredLimited\VaultSdk\Enum\Totp\Algorithm;
use FiguredLimited\VaultSdk\Enum\Totp\Digits;

class CreateForConsumerByKeyRequest implements CreateRequestInterface
{
    public function __construct(
        private readonly string $key,
        private string $issuer = '',
        private string $accountName = '',
        private int $periodSeconds = 30,
        private Algorithm $algorithm = Algorithm::SHA1,
        private Digits $digits = Digits::SIX,
    ) {
    }

    public function getIssuer(): string
    {
        return $this->issuer;
    }

    public function setIssuer(string $issuer): CreateForConsumerByKeyRequest
    {
        $this->issuer = $issuer;

        return $this;
    }

    public function getAccountName(): string
    {
        return $this->accountName;
    }

    public function setAccountName(string $accountName): CreateForConsumerByKeyRequest
    {
        $this->accountName = $accountName;

        return $this;
    }

    public function getPeriodSeconds(): int
    {
        return $this->periodSeconds;
    }

    public function setPeriodSeconds(int $periodSeconds): CreateForConsumerByKeyRequest
    {
        $this->periodSeconds = $periodSeconds;

        return $this;
    }

    public function getAlgorithm(): Algorithm
    {
        return $this->algorithm;
    }

    public function setAlgorithm(Algorithm $algorithm): CreateForConsumerByKeyRequest
    {
        $this->algorithm = $algorithm;

        return $this;
    }

    public function getDigits(): Digits
    {
        return $this->digits;
    }

    public function setDigits(Digits $digits): CreateForConsumerByKeyRequest
    {
        $this->digits = $digits;

        return $this;
    }

    public function toPayload(): array
    {
        return [
            'generate'     => false,
            'key'          => $this->key,
            'issuer'       => $this->issuer,
            'algorithm'    => $this->algorithm,
            'period'       => $this->periodSeconds,
            'digits'       => $this->digits->value,
            'account_name' => $this->accountName,
        ];
    }
}
