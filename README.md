# Vault PHP SDK

This library allows using hashicorp vault from PHP applications. It's a thin layer around the HTTP API provided by vault.

## Installation

Install the library with composer. 

```shell
composer config repositories.vault-php-sdk vcs https://gitlab.com/figured/open-source/vault-php-sdk.git 
composer require figured/vault-php-sdk
```

The library requires a PSR7 compliant HTTP client to be installed (for example `guzzlehttp/guzzle` and `guzzlehttp/psr7`)

## Authentication

Currently, the only supported authentication method is token based authentication.

## Secrets engines

### Transit engine

The Transit engine in Vault is for using vault for encryption as a service.

Example usage:

```php
$secret = 'foo';

$client = new \FiguredLimited\VaultSdk\Client($httpClient, $requestFactory, $streamFactory, 'http://localhost:8200', 's.secret');
$transit = new \FiguredLimited\VaultSdk\Engine\Transit($client);
$ciphertext = $transit->encrypt('keyName', $secret);

echo $transit->decrypt('keyName', $ciphertext); // foo
```

### TOTP engine

The TOTP engine in Vault is for using time based one time passwords, typically used for multifactor authentication using 
an authenticator app.

At creation time there are 2 kinds of keys Vault supports: consumer or producer keys. Consumer keys are meant to be used
by a client in place of an athenticator app (like Google Authenticator), while producer keys are meant to be used to
authenticate users from the server side. This distinction only applies at key creation time only, after creation all 
keys can be used for either purposes.

All 3 request objects can be created either directly or using the `\FiguredLimited\VaultSdk\Factory\Request\Totp\CreateRequestFactory`

#### Creating a consumer key

There are 2 ways to create a consumer key:
* By URL
* By key and arguments

The return value for consumer key creation will always be NULL.

By URL:
```php
$url = 'otpauth://totp/Google:test@gmail.com?secret=Y64VEVMBTSXCYIWRSHRNDZW62MPGVU2G&issuer=Google'; 
$client = new \FiguredLimited\VaultSdk\Client($httpClient, $requestFactory, $streamFactory, 'http://localhost:8200', 's.secret');
$totp = new \FiguredLimited\VaultSdk\Engine\Totp($client);
$request = new \FiguredLimited\VaultSdk\Request\Totp\CreateForConsumerByUrlRequest($url);

$totp->createKey('testKey', $request);
```

By key and arguments:
```php
$url = 'otpauth://totp/Google:test@gmail.com?secret=Y64VEVMBTSXCYIWRSHRNDZW62MPGVU2G&issuer=Google'; 
$client = new \FiguredLimited\VaultSdk\Client($httpClient, $requestFactory, $streamFactory, 'http://localhost:8200', 's.secret');
$totp = new \FiguredLimited\VaultSdk\Engine\Totp($client);
$request = new \FiguredLimited\VaultSdk\Request\Totp\CreateForConsumerByKeyRequest('Y64VEVMBTSXCYIWRSHRNDZW62MPGVU2G', 'Google', 'test@gmail.com');

$totp->createKey('testKey', $request);
```

#### Creating a producer key

```php
$url = 'otpauth://totp/Google:test@gmail.com?secret=Y64VEVMBTSXCYIWRSHRNDZW62MPGVU2G&issuer=Google'; 
$client = new \FiguredLimited\VaultSdk\Client($httpClient, $requestFactory, $streamFactory, 'http://localhost:8200', 's.secret');
$totp = new \FiguredLimited\VaultSdk\Engine\Totp($client);
$request = new \FiguredLimited\VaultSdk\Request\Totp\CreateForProducerRequest('yourCompanyName', 'test@example.com');

$result = $totp->createKey('testKey', $request);

echo '<p>URL: ' . $result->url .'<br><img src="' . $result->getBarcodeDataUri()"></p>';
```

The return value of a producer create request will contain the URL and the barcode normally. However, if `exported` is 
FALSE, the return value will be NULL, and if `qrSize` is `0`, the barcode will be empty.

#### Generating a key for a consumer

```php
$client = new \FiguredLimited\VaultSdk\Client($httpClient, $requestFactory, $streamFactory, 'http://localhost:8200', 's.secret');
$totp = new \FiguredLimited\VaultSdk\Engine\Totp($client);

$code = $totp->generateCode('testKey'); // string with the generated key
```

#### Verifying a code for a producer

```php
$client = new \FiguredLimited\VaultSdk\Client($httpClient, $requestFactory, $streamFactory, 'http://localhost:8200', 's.secret');
$totp = new \FiguredLimited\VaultSdk\Engine\Totp($client);

$code = $totp->verifyCode('testKey', '123456'); // TRUE if the code is correct, FALSE otherwise 
```

The `verifyCode` method may throw a `\FiguredLimited\VaultSdk\Exception\Totp\CodeAlreadyUsedException` exception if the 
same code is used twice (most likely a replay attack). In this case prompting the user to try again with the next 
generated code would be the most appropriate action.
