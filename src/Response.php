<?php

declare(strict_types=1);
namespace FiguredLimited\VaultSdk;

use Psr\Http\Message\ResponseInterface;

class Response
{
    protected string $requestId;
    protected string $leaseId;
    protected bool $renewable;
    protected int $leaseDurationSeconds;
    protected array $data;
    protected ?string $wrapInfo;
    protected ?array $warnings;
    protected ?array $auth;

    public function __construct(protected ResponseInterface $rawResponse)
    {
        $body = json_decode((string) $this->rawResponse->getBody(), true);

        $this->requestId            = $body['request_id'];
        $this->leaseId              = $body['lease_id'];
        $this->renewable            = $body['renewable'];
        $this->leaseDurationSeconds = $body['lease_duration'];
        $this->data                 = $body['data'];
        $this->wrapInfo             = $body['wrap_info'];
        $this->warnings             = $body['warnings'];
        $this->auth                 = $body['auth'];
    }

    public function getRawResponse(): ResponseInterface
    {
        return $this->rawResponse;
    }

    public function getRequestId(): string
    {
        return $this->requestId;
    }

    public function getLeaseId(): string
    {
        return $this->leaseId;
    }

    public function isRenewable(): bool
    {
        return $this->renewable;
    }

    public function getLeaseDurationSeconds(): int
    {
        return $this->leaseDurationSeconds;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getWrapInfo(): ?string
    {
        return $this->wrapInfo;
    }

    public function getWarnings(): ?array
    {
        return $this->warnings;
    }

    public function getAuth(): ?array
    {
        return $this->auth;
    }
}
