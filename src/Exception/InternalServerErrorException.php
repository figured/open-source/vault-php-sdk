<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Exception;

class InternalServerErrorException extends ClientException
{
}
