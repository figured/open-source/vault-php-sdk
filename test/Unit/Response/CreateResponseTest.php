<?php
declare(strict_types=1);
namespace FiguredLimited\VaultSdk\Test\Unit\Response;

use FiguredLimited\VaultSdk\Exception\Totp\NoBarcodeInResponseException;
use FiguredLimited\VaultSdk\Response\Totp\CreateResponse;
use PHPUnit\Framework\TestCase;

class CreateResponseTest extends TestCase
{
    public function testGetBarcodeImageContentWithNullBarcode_shouldThrowException(): void
    {
        $this->expectException(NoBarcodeInResponseException::class);

        $sut = new CreateResponse('test', null);

        $sut->getBarcodeImageContent();
    }

    public function testGetBarcodeDataUriWithNullBarcode_shouldThrowException(): void
    {
        $this->expectException(NoBarcodeInResponseException::class);

        $sut = new CreateResponse('test', null);

        $sut->getBarcodeDataUri();
    }

    public function testGetBarcodeImageContentWithNullBarcode_shouldReturnUri(): void
    {
        $barcode = base64_encode('testUrl');
        $sut     = new CreateResponse('test', $barcode);

        $this->assertSame('testUrl', $sut->getBarcodeImageContent());
    }

    public function testGetBarcodeDataUriWithNullBarcode_shouldReturnDecoded(): void
    {
        $barcode = base64_encode('testUrl');
        $sut     = new CreateResponse('test', $barcode);

        $this->assertSame('data:image/png;base64,' . $barcode, $sut->getBarcodeDataUri());
    }
}
